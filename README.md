## Instructions 🍕🍕🍕

The file "index.js" contains a variable called "BASE_URL". In this case this variable assign the value 
"https://pizzaproject.localhost.com/" because is my virtualhost project; when you mount the project in 
the server is necessary change the URL for the URL you used it. The only important thing is the URL need 
to end in "/".
