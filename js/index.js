function showError(message) {
    alert("ERROR: " + message);
}

const BASE_URL      = "https://pizzaproject.localhost.com/";
const SERVICES_URL  = BASE_URL + "services/";
const Services      = {};

Object.defineProperty(Services, "process", { value: SERVICES_URL + "process", writable: false });

function isValidJSON(str){
    if(typeof str !== "string"){
        str = JSON.stringify(str);
    }
    try{
        JSON.parse(str);
    }catch(ex){
        return false;
    }
    return true;
}

function showErrorMessage(message){
    Swal.fire({
        allowOutsideClick: false,
        showConfirmButton: false,
        showCancelButton: true,
        icon: "error",
        title: "Error!",
        text: message,
        cancelButtonText: `<i class="fas fa-times me-2"></i>Cerrar`
    });
}

function showSuccessMessage(message){
    Swal.fire({
        allowOutsideClick: false,
        showConfirmButton: false,
        showCancelButton: true,
        icon: "success",
        title: "Success!",
        text: message,
        cancelButtonText: `<i class="fas fa-times me-2"></i>Cerrar`
    });
}

function hideInputError($input){
    if(typeof $input !== "undefined" && $input.length !== 0){
        $input.removeClass("is-invalid");
        let inputId = $input.attr("id");
        let feedbackInputId = "validation" + inputId + "Feedback";
        if($("#"+feedbackInputId).length !== 0){
            $("#"+feedbackInputId).remove();
        }
    }
}

function showInputError($input, message = "Input value error."){
    if(typeof $input !== "undefined" && $input.length !== 0){
        $input.removeClass("is-invalid");
        let inputId = $input.attr("id");
        if(inputId.length === 0){
            throw "The input should be have an \"id\" attribute.";
        }
        let feedbackInputId = "validation" + inputId + "Feedback";
        if($("#"+feedbackInputId).length !== 0){
            $("#"+feedbackInputId).remove();
        }
        let $div = $("<div />", { "id": feedbackInputId, "class": "invalid-feedback", "html": message });
        $input.after($div);
        $input.addClass("is-invalid");
    }
}

function refreshListToppings($listToppings, data){
    let $tbody = $listToppings.find("tbody");
    $tbody.html("");
    $.each(data, function(key, item){
        let $tr = $("<tr />", { "data-id": item.id });
        
        let $th = $("<th />", { "scope": "row" });
        $th.html(item.topping);
        $tr.append($th);

        let $td = $("<td />");
        let $div = $("<div />", { "class": "align-baseline text-center" });
        let $i = $("<i />", { "class": "fas fa-trash-alt" });
        let $span = $("<span />", { "class": "invisible d-none", "html": "Delete" });
        let $button = $("<button />", { "class": "btnDeleteTopping btn btn-sm btn-danger", "title": "Delete" });
        $button.append($i).append($span);
        $div.append($button);

        $td.append($div);
        $tr.append($td);
        $tbody.append($tr);
    });
    $listToppings.find("tfoot tr td #totalToppings").html(data.length);
}

const Pizza = {
    topping: "",
    toppingList: [],

    /**
     * Setters y Getters.
     */
    setTopping: function(topping){
        this.topping = topping;
    },
    getTopping: function(){
        return this.topping;
    },
    setToppingList(toppingList){
        this.toppingList = toppingList;
    },
    getToppingList(){
        return this.toppingList;
    },
    isValidTopping: function(topping){
        let data = topping;
        data = data.trim();
        /**
         * APA: Coyier, C. (2009). Strip HTML Tags in JavaScript, Recuperado el 18 de noviembre del 2021 de https://css-tricks.com/snippets/javascript/strip-html-tags-in-javascript/
         */
        data = data.replace(/(<([^>]+)>)/gi, "");
        return (data.length !== 0);
    },

    /**
     * AJAX event for working with server data.
     */
    getToppings: function(jsonData){
        return new Promise((resolve, reject) => {
            $.ajax({
                "cache": false,
                "method": "post",
                "url": Services.process,
                "data": jsonData,
                "beforeSend": function(jqXHR, settings){}
            }).done(function(data, textStatus, jqXHR){
                resolve(data);
            }).fail(function(jqXHR, textStatus, errorThrown){
                reject(jqXHR);
            });
        });
    },
    addTopping: function(jsonData){
        return new Promise((resolve, reject) => {
            $.ajax({
                "cache": false,
                "method": "post",
                "url": Services.process,
                "data": jsonData,
                "beforeSend": function(jqXHR, settings){}
                // "url": 'index.php?action=addTopping',
                // "data": { topping: $("#topping").val() },
                /*
                success: function(result) {
                    try {
                        json = jQuery.parseJSON(result);
                        console.log(json);
                    } catch (e) {
                        showError("Invalid JSON returned from server: " + result);
                        return;
                    }
                    if (json["success"] === 0) {
                        showError(json["errormsg"]);
                    } else {
                        $("#topping").val("");
                        getToppings();
                    }
                },
                error: function(){
                    showError('Error Reaching index.php');
                }
                */
            }).done(function(data, textStatus, jqXHR){
                resolve(data);
            }).fail(function(jqXHR, textStatus, errorThrown){
                reject(jqXHR);
            });
        });
    },
    deleteTopping: function(jsonData, toppingId){
        return new Promise((resolve, reject) => {
            $.ajax({
                "cache": false,
                "method": "post",
                "url": Services.process,
                "data": jsonData,
                "beforeSend": function(jqXHR, settings){}
            }).done(function(data, textStatus, jqXHR){
                resolve(data);
            }).fail(function(jqXHR, textStatus, errorThrown){
                reject(jqXHR);
            });
        });
        /*
        console.log(toppingId);
    
        $.ajax({
            url: 'index.php?action=deleteTopping&toppingId='+toppingId,
            dataType: 'JSON',
            success: function(result) {
    
                if(result.success === 0){
                    showError(result.message);
                }else{
                    getToppings();
                }
            },
            error: function(xhr) {
                console.log(xhr);
                showError('Error Reaching Server');
            }
    
        });
        */
    }
};

function pageAddTopping(){
    let $frmAddToppings = $("#frmAddToppings");
    let $inputTopping   = $frmAddToppings.find("#inputTopping");
    let $btnAddTopping  = $frmAddToppings.find("#btnAddTopping");
    let $listToppings   = $("#listToppings");

    $btnAddTopping.off("click").on("click", function(event){
        event.preventDefault();
        let inputTopping = $inputTopping.val();
        hideInputError($inputTopping);
        if(!Pizza.isValidTopping(inputTopping)){
            showInputError($inputTopping, "You should provide a pizza toppig!!!");
            return false;
        }else if(Pizza.isValidTopping(inputTopping)){
            inputTopping = inputTopping.trim();
            inputTopping = inputTopping.replace(/(<([^>]+)>)/gi, "");
            Pizza.setTopping(inputTopping);
            let jsonData = {
                "action": "addTopping",
                "topping": Pizza.getTopping()
            };
            Pizza.addTopping(jsonData).then((data) => {
                let response = data;
                if(!isValidJSON(response)){
                    showErrorMessage("Invalid JSON returned from server");
                }else if(response.status === "error"){
                    Pizza.setToppingList(response.data);
                    showErrorMessage(response.message);
                }else if(response.status === "success"){
                    $inputTopping.val("");
                    Pizza.setTopping(null);
                    Pizza.setToppingList(response.data);
                    refreshListToppings($listToppings, Pizza.getToppingList());
                    showSuccessMessage(response.message);
                }else{
                    showErrorMessage("Something went wrong, refresh the page and try again.");
                }
            }).catch((error) => {
                showErrorMessage("Something went wrong, refresh the page and try again.");
            }).finally(function(){});
        }
        return false;
    });

    $(document).on("click", ".btnDeleteTopping", function(event){
        event.preventDefault();
        let $element = $(this);
        let id = $element.closest("tr").attr("data-id");
        id = id.trim();
        if(id.length !== 0){
            let jsonData = {
                "action": "deleteTopping",
                "id": id
            };
            Pizza.deleteTopping(jsonData).then((data) => {
                let response = data;
                if(!isValidJSON(response)){
                    showErrorMessage("Invalid JSON returned from server, refresh the page.");
                }else if(response.status === "error"){
                    Pizza.setToppingList(response.data);
                    showErrorMessage(response.message);
                }else if(response.status === "success"){
                    $inputTopping.val("");
                    Pizza.setTopping(null);
                    Pizza.setToppingList(response.data);
                    refreshListToppings($listToppings, Pizza.getToppingList());
                    showSuccessMessage(response.message);
                }else{
                    showErrorMessage("Something went wrong, refresh the page and try again.");
                }
            }).catch((error) => {
                showErrorMessage("Something went wrong, refresh the page and try again.");
            }).finally(function(){});
        }
        return false;
    });
    
    let jsonData = {
        "action": "getToppings"
    };
    Pizza.getToppings(jsonData).then((data) => {
        let response = data;
        if(!isValidJSON(response)){
            showErrorMessage("Invalid JSON returned from server, refresh the page.");
        }else if(response.status === "success"){
            Pizza.setToppingList(response.data);
            refreshListToppings($listToppings, Pizza.getToppingList());
        }else{
            showErrorMessage("Something went wrong, refresh the page and try again.");
        }
    }).catch((error) => {
        showErrorMessage("Something went wrong, refresh the page and try again.");
    }).finally(function(){});
}

$(document).ready(function(){
    if($("#app").length !==0){
        if($("div[data-page=\"addTopping\"]").length !== 0){
            pageAddTopping();
        }
    }
});