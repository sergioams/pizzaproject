<?php
session_start();

if(!isset($_POST["action"])){
    $_POST["action"] = "";
}

if(!isset($_SESSION["toppings"])){
    $_SESSION["toppings"] = [];
}

$actionsAllowed = ["getToppings", "deleteTopping", "addTopping"];
if(in_array($_POST["action"], $actionsAllowed)){
    switch($_POST['action']){
        case 'getToppings':
            $response = [
                "status"    => "success",
                "message"   => "The action has been carried out.",
                "data"      => []
            ];
            if(isset($_SESSION['toppings'])){
                $response["data"] = $_SESSION['toppings'];
            }
            http_response_code(200);
            header("Content-type: application/json; charset=utf-8");
            echo json_encode($response);
            exit();
        break;
        case 'deleteTopping':
            $response = [
                "status"    => "error",
                "message"   => "The action could not be performed.",
                "data"      => []
            ];
            try{
                $totalToppingsBefore = count($_SESSION['toppings']);
                $totalToppings = count($_SESSION['toppings']);
                if($totalToppings !== 0){
                    $id = strip_tags($_POST["id"]);
                    $id = trim($_POST["id"]);
                    $foundKey = array_search($id, array_column($_SESSION["toppings"], "id"));
                    if($foundKey === false){
                        $response["status"] = "error";
                        $response["message"]= "The topping was not exist";
                    }else{
                        unset($_SESSION['toppings'][$foundKey]);
                        $totalToppings = count($_SESSION['toppings']);
                        if($totalToppings < $totalToppingsBefore){
                            $response["status"]     = "success";
                            $response["message"]    = "The topping was removed";
                            $_SESSION['toppings']   = array_values($_SESSION['toppings']);
                        }else{
                            $response["message"] = "The topping was not removed";
                        }
                    }
                    $response["data"] = $_SESSION['toppings'];
                    http_response_code(200);
                    header("Content-type: application/json; charset=utf-8");
                    echo json_encode($response);
                    exit();
                }
            }catch(Exception $ex){
                http_response_code(500);
                $response["status"] = "error";
                $response["message"]= $ex->getMessage();
                $response["data"]   = $_SESSION['toppings'];
                header("Content-type: application/json; charset=utf-8");
                echo json_encode($response);
                exit();
            }
        break;
        case 'addTopping':
            $response = [
                "status"    => "error",
                "message"   => "The action could not be performed.",
                "data"      => []
            ];
            try{
                if(isset($_POST['topping']) && strlen(str_replace(" ", "", $_POST['topping'])) > 0){
                    $_POST["topping"] = strip_tags($_POST["topping"]);
                    $_POST["topping"] = trim($_POST["topping"]);
                    $_SESSION['toppings'][] = [
                        "id"        => uniqid(),
                        "topping"   => $_POST["topping"]
                    ];
                    $response["status"] = "success";
                    $response["message"]= "The topping added successfully.";
                }else{
                    $response["status"] = "error";
                    $response["message"]= "No Topping Entered.";
                }
                $response["data"] = $_SESSION['toppings'];
                http_response_code(200);
                header("Content-type: application/json; charset=utf-8");
                echo json_encode($response);
                exit();
            }catch(Exception $ex){
                http_response_code(500);
                $response["status"] = "error";
                $response["message"]= $ex->getMessage();
                $response["data"]   = $_SESSION['toppings'];
                header("Content-type: application/json; charset=utf-8");
                echo json_encode($response);
                exit();
            }
        break;
    }
}
