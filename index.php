<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Pizza Pizza</title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <meta name="description" content="Welcome to the Mendoza Corporation, PIZZA code exercise.">
    <meta name="author" content="Sergio Alberto Méndez Sánchez">
    <meta name="copyright" content="Sergio Alberto Méndez Sánchez" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&display=swap">
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="css/styles.css?v=<?php echo time(); ?>">
</head>
<body>
<div id="app">
	<div data-page="addTopping">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-lg-6">
					<div class="card mt-5">
						<div class="card-body">
							<div class="mx-3 my-1">
								<form id="frmAddToppings" method="post" autocomplete="off">
									<div class="mb-3">
										<label for="topping" class="form-label">
											What topping would you like?
										</label>
										<input id="inputTopping" name="inputTopping" type="text" class="form-control" value="" autofocus="autofocus" />
									</div>
									<div>
										<button id="btnAddTopping" type="button" class="btn btn-success">
											<i class="fas fa-plus me-2"></i>Add it!
										</button>
									</div>
									<div id="g-alert-message"></div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row d-flex justify-content-center">
				<div class="col-lg-8">
					<hr />
				</div>
			</div>

			<div class="row d-flex justify-content-center">
				<div class="col-lg-6">
					<div class="card">
						<div class="card-body">
							<table id="listToppings" class="table table-sm table-bordered table-striped border border-dark m-0">
								<thead class="bg-warning">
									<tr>
										<th scope="col">
											Pizza's Topping
										</th>
										<th scope="col" class="text-center">
											Actions
										</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="2">
											<strong class="me-2">Total:</strong><span id="totalToppings"></span>
										</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<img id="bg" src="img/bg-pizza.jpg" alt="bg-pizza" />
<script type="application/javascript" src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="application/javascript" src="vendor/sweetalert2/sweetalert2.min.js"></script>
<script type="application/javascript" src="js/jquery.min.js"></script>
<script type="application/javascript" src="js/index.js?v=<?php echo time(); ?>"></script>
</body>
</html>
